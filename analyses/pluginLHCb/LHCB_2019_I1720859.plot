BEGIN PLOT /LHCB_2019_I1720859/d01-x01-y01
Title=LHCb, 13 TeV
XLabel=$p_{T} (H_b)$ [GeV]
YLabel=$B^0_s$ fraction
YMin=0.0
YMax=0.5
LogY=0
END PLOT

BEGIN PLOT /LHCB_2019_I1720859/d01-x01-y02
Title=LHCb, 13 TeV
XLabel=$p_{T} (H_b)$ [GeV]
YLabel=$\Lambda^0_b$ fraction
YMin=0.0
YMax=0.7
LogY=0
END PLOT