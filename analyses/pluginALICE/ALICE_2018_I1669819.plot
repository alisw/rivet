BEGIN PLOT /ALICE_2018_I1669819/d01-x01-y01
Title=Prompt D$^{0}$ mesons in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d02-x01-y01
Title=Prompt D$^{+}$ mesons in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d03-x01-y01
Title=Prompt D$^{*+}$ mesons in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d04-x01-y01
Title=Prompt D$^{+}_{s}$ mesons in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d05-x01-y01
Title=Prompt D$^{0}$ mesons in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d06-x01-y01
Title=Prompt D$^{+}$ mesons in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d07-x01-y01
Title=Prompt D$^{*+}$ mesons in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d08-x01-y01
Title=Prompt D$^{+}_{s}$ mesons in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d09-x01-y01
Title=Prompt D$^{0}$ mesons in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d10-x01-y01
Title=Prompt D$^{+}$ mesons in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d11-x01-y01
Title=Prompt D$^{*+}$ mesons in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d12-x01-y01
Title=Prompt D$^{+}_{s}$ mesons in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$d$N/$dp_{T}$ (GeV/$c$)$^{-1}$
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d13-x01-y01
Title=Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d14-x01-y01
Title=Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{*+}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d15-x01-y01
Title=Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}_{s}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d16-x01-y01
Title=Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}_{s}$/D$^{+}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d17-x01-y01
Title=Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d18-x01-y01
Title=Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{*+}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d19-x01-y01
Title=Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}_{s}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d20-x01-y01
Title=Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}_{s}$/D$^{+}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d21-x01-y01
Title=Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d22-x01-y01
Title=Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{*+}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d23-x01-y01
Title=Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}_{s}$/D$^{0}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d24-x01-y01
Title=Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=D$^{+}_{s}$/D$^{+}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d25-x01-y01
Title=Prompt D$^{0}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d26-x01-y01
Title=Prompt D$^{+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d27-x01-y01
Title=Prompt D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d28-x01-y01
Title=Prompt D$^{+}_{s}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d29-x01-y01
Title=Average of prompt D$^{0}$ D$^{+}$ D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d30-x01-y01
Title=Prompt D$^{0}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d31-x01-y01
Title=Prompt D$^{+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d32-x01-y01
Title=Prompt D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d33-x01-y01
Title=Prompt D$^{+}_{s}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d34-x01-y01
Title=Average of prompt D$^{0}$ D$^{+}$ D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d35-x01-y01
Title=Prompt D$^{0}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d36-x01-y01
Title=Prompt D$^{+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d37-x01-y01
Title=Prompt D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d38-x01-y01
Title=Prompt D$^{+}_{s}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d39-x01-y01
Title=Average of prompt D$^{0}$ D$^{+}$ D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d40-x01-y01
Title=Average of prompt D$^{0}$ D$^{+}$ D$^{*+}$ $R_\mathrm{TAA}$ in Pb-Pb at 2.76 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d41-x01-y01
Title=Pb-Pb at 5.02 TeV (0-10\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$(D$^{0}$ D$^{+}$ D$^{*+}$)/$R_\mathrm{TAA}$(ch. particles)
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d42-x01-y01
Title=Pb-Pb at 5.02 TeV (30-50\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$(D$^{0}$ D$^{+}$ D$^{*+}$)/$R_\mathrm{TAA}$(ch. particles)
LogY=0
END PLOT

BEGIN PLOT /ALICE_2018_I1669819/d43-x01-y01
Title=Pb-Pb at 5.02 TeV (60-80\%)
XLabel=$p_\mathrm{TT}$ (GeV/$c$)
YLabel=$R_\mathrm{TAA}$(D$^{0}$ D$^{+}$ D$^{*+}$)/$R_\mathrm{TAA}$(ch. particles)
LogY=0
END PLOT
