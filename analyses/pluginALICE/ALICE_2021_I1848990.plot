BEGIN PLOT /ALICE_2021_I1848990/d01-x01-y01
Title=Differential cross section of non-prompt $D^0$ production
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)$ [$\mu$b/(GeV/c)]
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d02-x01-y01
Title=Differential cross section of non-prompt $D^+$ production
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)$ [$\mu$b/(GeV/c)]
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d03-x01-y01
Title=Differential cross section of non-prompt $D_S^{*+}$ production
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)$ [$\mu$b/(GeV/c)]
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d04-x01-y01
Title=Differential cross section of prompt $D^0$ production at $|y| < 0.5$.
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)$ [$\mu$b/(GeV/c)]
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d05-x01-y01
Title=Differential cross section prompt $D^+$ production at $|y| < 0.5$.
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)$ [$\mu$b/(GeV/c)]
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d06-x01-y01
Title=Differential cross section of prompt $D_S^{*+}$ production at $|y| < 0.5$.
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)$ [$\mu$b/(GeV/c)]
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d07-x01-y01
Title=Ratio of differential cross section of non-prompt and prompt $D^{0}$
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)_{\text{non-prompt}}/\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)_{\text{prompt}}$ 
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d08-x01-y01
Title=Ratio of differential cross section of non-prompt and prompt $D^{*+}$
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)_{\text{non-prompt}}/\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)_{\text{prompt}}$ 
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d09-x01-y01
Title=Ratio of differential cross section of non-prompt and prompt $D_S^{*+}$
XLabel=$p_T$ [GeV/$c$]
YLabel=$\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)_{\text{non-prompt}}/\mathrm{d^2}\sigma/(\mathrm{d}p_T\mathrm{d}y)_{\text{prompt}}$ 
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d10-x01-y01
Title=Ratio of differential cross section prompt $D^{+}$ and $D^{0}$ 
XLabel=$p_T$ [GeV/$c$]
YLabel=D$^{+}/$D$^{0}$
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d11-x01-y01
Title=Ratio of differential cross section non-prompt $D^{+}$ and $D^{0}$ 
XLabel=$p_T$ [GeV/$c$]
YLabel=D$^{+}/$D$^{0}$
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d12-x01-y01
Title=Ratio of differential cross section prompt $D_S^{*+}$ and ($D^{+}$+$D^{0}$) 
XLabel=$p_T$ [GeV/$c$]
YLabel=D$_S^{*+}/$(D$^{+}+$D$^{0}$)
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d13-x01-y01
Title=Ratio of differential cross section non-prompt $D_S^{*+}$ and ($D^{+}$+$D^{0}$)
XLabel=$p_T$ [GeV/$c$]
YLabel=D$_S^{*+}/$(D$^{+}+$D$^{0}$)
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d14-x01-y01
Title=Charm-quark fragmentation-fraction ratio for $p_T(D)>1$ GeV/$c$
XCustomMajorTicks=1	"pp $\to$ D (Q=PROMPT) + X"
YLabel=prompt $f_s$/($f_u+f_d$)
ConnectBins=0
END PLOT

BEGIN PLOT /ALICE_2021_I1848990/d15-x01-y01
Title=Beauty-quark fragmentation-fraction ratio for $p_T(D)>2$ GeV/$c$
XCustomMajorTicks=1	"pp $\to$ D (Q=NON-PROMPT) + X"
YLabel=non-prompt $f_s$/($f_u+f_d$)
ConnectBins=0
END PLOT


