BEGIN PLOT /ALICE_2021_I1946131/d01-x01-y01
Title=Transverse momentum of prompt $D^{0}$ ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{0}}$ [GeV/c]
YLabel=$\frac{\mathrm{Td}^{2}N}{\mathrm{Tdp}_{\mathrm{TT}} \mathrm{Td}y}$ [1/(GeV/c)]
YLabelSep=6.3
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d02-x01-y01
Title=Transverse momentum of prompt $D^{0}$ ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{0}}$ [GeV/c]
YLabel=$\frac{\mathrm{Td}^{2}N}{\mathrm{Tdp}_{\mathrm{TT}} \mathrm{Td}y}$ [1/(GeV/c)]
YLabelSep=6.3
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d03-x01-y01
Title=Transverse momentum of prompt $D^{+}$ ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{+}}$ [GeV/c]
YLabel=$\frac{\mathrm{Td}^{2}N}{\mathrm{Tdp}_\mathrm{TT} \mathrm{Td}y}$ [1/(GeV/c)]
YLabelSep=6.3
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d04-x01-y01
Title=Transverse momentum of prompt $D^{+}$ ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{+}}$ [GeV/c]
YLabel=$\frac{\mathrm{Td}^{2}N}{\mathrm{Tdp}_{\mathrm{TT}} \mathrm{Td}y}$ [1/(GeV/c)]
YLabelSep=6.3
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d05-x01-y01
Title=Transverse momentum of prompt $D^{*+}$ ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{*+}}$ [GeV/c]
YLabel=$\frac{\mathrm{Td}^{2}N}{\mathrm{Tdp}_{\mathrm{TT}} \mathrm{Td}y}$ [1/(GeV/c)]
YLabelSep=6.3
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d06-x01-y01
Title=Transverse momentum of prompt $D^{*+}$ ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{*+}}$ [GeV/c]
YLabel= $\frac{\mathrm{Td}^{2}N}{\mathrm{Tdp}_{\mathrm{TT}} \mathrm{Td}y}$ [1/(GeV/c)]
YLabelSep=6.3
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d07-x01-y01
Title=Ratio $D^{+}/D^{0}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $\frac{\mathrm{TN}(D^{+})}{\mathrm{TN}(D^{0})}$
YLabelSep=4.5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d08-x01-y01
Title=Ratio $D^{+}/D^{0}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($30-50$\%) 
XLabel=$\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $\frac{\mathrm{TN}(D^{+})}{\mathrm{TN}(D^{0})}$
YLabelSep=4.5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d09-x01-y01
Title=Ratio $D^{*+}/D^{0}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $\frac{\mathrm{TN}(D^{*+})}{\mathrm{TN}(D^{0})}$
YLabelSep=4.5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d10-x01-y01
Title=Ratio $D^{*+}/D^{0}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $\frac{\mathrm{TN}(D^{*+})}{\mathrm{TN}(D^{0})}$
YLabelSep=4.5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d11-x01-y01
Title=$R_\mathrm{TAA}$ of prompt $D^{0}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{0}}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{0}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d12-x01-y01
Title=$R_\mathrm{TAA}$ of prompt $D^{+}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{+}}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{+}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d13-x01-y01
Title=$R_\mathrm{TAA}$ of prompt $D^{*+}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{*+}}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{*+}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d14-x01-y01
Title=$R_\mathrm{TAA}$ of $D^{0}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{0}}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{0}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d15-x01-y01
Title=$R_\mathrm{TAA}$ of prompt $D^{+}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{+}}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{+}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d16-x01-y01
Title=$R_\mathrm{TAA}$ of prompt $D^{*+}$ at $\sqrt{s_\mathrm{TNN}} = 5.02$ TeV ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}^{D^{*+}}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{*+}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d17-x01-y01
Title=Average $R_\mathrm{TAA}$ of prompt $D^{0},D^{+}$ and $D^{*+}$ ($0-10$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $<R_{AA}>$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d18-x01-y01
Title=Average $R_\mathrm{TAA}$ of prompt $D^{0},D^{+}$ and $D^{*+}$ ($30-50$\%)
XLabel=$\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $<R_{AA}>$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d19-x01-y01
Title=$p_\mathrm{TT}-$integrated $R_\mathrm{TAA}$ of prompt $D^{0}$ ($0-10$\%)
XLabel=Integrated $\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{0}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT

BEGIN PLOT /ALICE_2021_I1946131/d20-x01-y01
Title=$p_\mathrm{TT}-$integrated $R_\mathrm{TAA}$ of prompt $D^{0}$ ($30-50$\%)
XLabel=Integrated $\mathrm{Tp}_\mathrm{TT}$ [GeV/c]
YLabel= $R_\mathrm{TAA}$ $D^{0}$
YLabelSep=5
LegendXPos=0.5
LegendYPos=0.92
END PLOT


