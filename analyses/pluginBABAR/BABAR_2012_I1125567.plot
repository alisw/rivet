BEGIN PLOT /BABAR_2012_I1125567/d01-x01-y01
Title=$\pi^-K^0_SK^0_S$ mass distribution in $\tau^-\to\pi^-K^0_SK^0_S$
XLabel=$m_{\pi^-K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125567/d01-x01-y02
Title=$\pi^-K^0_S$ mass distribution in $\tau^-\to\pi^-K^0_SK^0_S$
XLabel=$m_{\pi^-K^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^-K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2012_I1125567/d01-x01-y03
Title=$K^0_SK^0_S$ mass distribution in $\tau^-\to\pi^-K^0_SK^0_S$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
