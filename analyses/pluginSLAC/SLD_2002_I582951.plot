# BEGIN PLOT /SLD_2002_I582951/d01-x01-y01
Title=$b$ quark fragmentation function $f(x_B^\mathrm{weak})$
XLabel=$x_B$
YLabel=$1/N \, \mathrm{d}{N}/\mathrm{d}x_B$
LegendYPos=0.60
LogY=0
# END PLOT
