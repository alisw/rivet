# BEGIN PLOT /LHCF_2023_I2658888/d01-x01-y01
Title=$pp\to\eta X$ in $p_\mathrm{T} < 1.10\,GeV/c$ at \sqrt{s}=13\,$TeV
XLabel=$x_\mathrm{F}$
YLabel=$\frac{ x_\mathrm{F}}{\sigma_\mathrm{inel}} \frac{\mathrm{d}\sigma}{\mathrm{d}x_\mathrm{F}}$
LegendXPos=0.75
LegendYPos=0.95
# END PLOT
