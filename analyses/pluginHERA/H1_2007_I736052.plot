BEGIN PLOT /H1_2007_I736052/d04-x01-y01
Title= $d\sigma /dQ^2$ for inclusive $D^{*\pm}$ production in the vis. kin. region
XLabel= $Q^2$ $[GeV^2]$
YLabel= $d\sigma /dQ^2$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d05-x01-y01
Title= $d\sigma /dx$ for inclusive $D^{*\pm}$ production in the vis. kin. region
XLabel= x
YLabel= $d\sigma /dx$ $[nb]$
LogX=1
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d06-x01-y01
Title= $d\sigma /dW$ for inclusive $D^{*\pm}$ production in the vis. kin. region
XLabel= $W$ $[GeV]$
YLabel= $d\sigma /dW$ $[nb$ $GeV^{-1}]$
LogX=0
LogY=0
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d07-x01-y01
Title= $d\sigma /dp_{T}$ for inclusive $D^{*\pm}$ production in the vis. kin. region
XLabel= $p_{T}$ $[GeV]$
YLabel= $d\sigma /dp_{T}$ $[nb$ $GeV^{-1}]$
LogX=0
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d08-x01-y01
Title= $d\sigma /d\eta$ for inclusive $D^{*\pm}$ production in the vis. kin. region
XLabel= $\eta$ 
YLabel= $d\sigma /d\eta$ $[nb]$
LogX=0
LogY=0
LegendXPos=0.05
LegendYPos=0.25
END PLOT

BEGIN PLOT /H1_2007_I736052/d09-x01-y01
Title= $d\sigma /dz$ for inclusive $D^{*\pm}$ production in the vis. kin. region
XLabel= z
YLabel= $d\sigma /dz$ $[nb]$
LogX=0
LogY=0
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d10-x01-y01
Title= $d^2\sigma /dQ^2dx$ for inclusive $D^{*\pm}$ production in bins of $Q^2$ and $x$
XLabel= x
YLabel= $d^2\sigma /dQ^2dx$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
Legend=1
CustomLegend= $Q^2\in[2,$ $4.22]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.3
END PLOT

BEGIN PLOT /H1_2007_I736052/d11-x01-y01
Title= $d^2\sigma /dQ^2dx$ for inclusive $D^{*\pm}$ production in bins of $Q^2$ and $x$
XLabel= x
YLabel= $d^2\sigma /dQ^2dx$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
Legend=1
CustomLegend= $Q^2\in[4.22,$ $10]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.3
END PLOT

BEGIN PLOT /H1_2007_I736052/d12-x01-y01
Title= $d^2\sigma /dQ^2dx$ for inclusive $D^{*\pm}$ production in bins of $Q^2$ and $x$
XLabel= x
YLabel= $d^2\sigma /dQ^2dx$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
Legend=1
CustomLegend= $Q^2\in[10,$ $17.8]$ $GeV^2$
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d13-x01-y01
Title= $d^2\sigma /dQ^2dx$ for inclusive $D^{*\pm}$ production in bins of $Q^2$ and $x$
XLabel= x
YLabel= $d^2\sigma /dQ^2dx$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
Legend=1
CustomLegend= $Q^2\in[17.8,$ $31.6]$ $GeV^2$
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d14-x01-y01
Title= $d^2\sigma /dQ^2dx$ for inclusive $D^{*\pm}$ production in bins of $Q^2$ and $x$
XLabel= x
YLabel= $d^2\sigma /dQ^2dx$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
Legend=1
CustomLegend= $Q^2\in[31.6,$ $100]$ $GeV^2$
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d15-x01-y01
Title= $d\sigma^* /dQ^2$ for inclusive $D^{*\pm}$ production in the $\gamma p$ cm frame
XLabel= $Q^2$ $[GeV^2]$
YLabel= $d\sigma^* /dQ^2$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
Legend=1
CustomLegend= $p_{T}$ $\textgreater$ $2$ $GeV$
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d16-x01-y01
Title= $d\sigma^* /dx$ for inclusive $D^{*\pm}$ production in the $\gamma p$ cm frame
XLabel= $x$ 
YLabel= $d\sigma^* /dx$ $[nb]$
LogX=1
LogY=1
Legend=1
CustomLegend= $p_{T}$ $\textgreater$ $2$ $GeV$
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d17-x01-y01
Title= $d\sigma^* /dp_{T}$ for inclusive $D^{*\pm}$ production in the $\gamma p$ cm frame
XLabel= $p_{T}$ $[GeV]$
YLabel= $d\sigma^* /dp_{T}$ $[nb$ $GeV^{-1}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $p_{T}$ $\textgreater$ $2$ $GeV$
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d18-x01-y01
Title= $d\sigma^* /d\eta$ for inclusive $D^{*\pm}$ production in the $\gamma p$ cm frame
XLabel= $\eta$ 
YLabel= $d\sigma^* /d\eta$ $[nb]$
LogX=0
LogY=0
Legend=1
CustomLegend= $p_{T}$ $\textgreater$ $2$ $GeV$
LegendXPos=0.05
LegendYPos=0.3
END PLOT

BEGIN PLOT /H1_2007_I736052/d19-x01-y01
Title= $d\sigma /dQ^2$ for $D^{*\pm}$ production with dijets (Breit frame) 
XLabel= $Q^2$ $[GeV^2]$
YLabel= $d\sigma /dQ^2$ $[nb$ $GeV^{-2}]$
LogX=1
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d20-x01-y01
Title= $d\sigma /dx$ for $D^{*\pm}$ production with dijets (Breit frame) 
XLabel= x
YLabel= $d\sigma /dx$ $[nb]$
LogX=1
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d21-x01-y01
Title= $d\sigma /dE^{max}_{T}$ for $D^{*\pm}$ production with dijets (Breit frame) 
XLabel= $E^{max}_T$ $[GeV]$
YLabel= $d\sigma /dE^{max}_{T}$ $[nb$ $GeV^{-1}]$
LogX=0
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d22-x01-y01
Title= $d\sigma /dM_{jj}$ for $D^{*\pm}$ production with dijets (Breit frame) 
XLabel= $M_{jj}$ $[GeV]$
YLabel= $d\sigma /dM_{jj}$ $[nb$ $GeV^{-1}]$
LogX=0
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d23-x01-y01
Title= $d^2\sigma /dQ^2d\Delta \phi$ for $D^{*\pm}$ production with dijets in bins of $Q^2$(Breit) 
XLabel= $\Delta \phi$ $[^{\circ}]$
YLabel= $d^2\sigma /dQ^2d\Delta \phi$ $[nb$ $GeV^{-2}$ $^{\circ} ^{-1}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[2,$ $10]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.9
END PLOT

BEGIN PLOT /H1_2007_I736052/d24-x01-y01
Title= $d^2\sigma /dQ^2d\Delta \phi$ for $D^{*\pm}$ production with dijets in bins of $Q^2$(Breit) 
XLabel= $\Delta \phi$ $[^{\circ}]$
YLabel= $d^2\sigma /dQ^2d\Delta \phi$ $[nb$ $GeV^{-2}$ $^{\circ} ^{-1}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[10,$ $100]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.9
END PLOT

BEGIN PLOT /H1_2007_I736052/d25-x01-y01
Title= $d\sigma /d\eta_{DJ}$ for $D^{*\pm}$ production with dijets 
XLabel= $\eta_{DJ}$
YLabel= $d\sigma /d\eta_{DJ}$ $[nb]$
LogX=0
LogY=0
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d26-x01-y01
Title= $d\sigma /d\eta_{OJ}$ for $D^{*\pm}$ production with dijets 
XLabel= $\eta_{OJ}$
YLabel= $d\sigma /d\eta_{OJ}$ $[nb]$
LogX=0
LogY=0
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d27-x01-y01
Title= $d\sigma /d\Delta \eta$ for $D^{*\pm}$ production with dijets 
XLabel= $\Delta \eta$
YLabel= $d\sigma /d\Delta \eta$ $[nb]$
LogX=0
LogY=0
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d28-x01-y01
Title= $d\sigma /dx^{obs}_{\gamma}$ for $D^{*\pm}$ production with dijets
XLabel= $x^{obs}_{\gamma}$
YLabel= $d\sigma /dx^{obs}_{\gamma}$ $[nb]$
LogX=0
LogY=0
LegendXPos=0.05
LegendYPos=0.9
END PLOT

BEGIN PLOT /H1_2007_I736052/d29-x01-y01
Title= $d^2\sigma /dQ^2dx^{obs}_{\gamma}$ for $D^{*\pm}$ production with dijets in bins of $Q^2$
XLabel= $x^{obs}_{\gamma}$
YLabel= $d^2\sigma /dQ^2dx^{obs}_{\gamma}$ $[nb$ $GeV^{-2}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[2,$ $5]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.9
END PLOT

BEGIN PLOT /H1_2007_I736052/d29-x01-y02
Title= $d^2\sigma /dQ^2dx^{obs}_{\gamma}$ for $D^{*\pm}$ production with dijets in bins of $Q^2$
XLabel= $x^{obs}_{\gamma}$
YLabel= $d^2\sigma /dQ^2dx^{obs}_{\gamma}$ $[nb$ $GeV^{-2}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[5,$ $10]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.9
END PLOT

BEGIN PLOT /H1_2007_I736052/d29-x01-y03
Title= $d^2\sigma /dQ^2dx^{obs}_{\gamma}$ for $D^{*\pm}$ production with dijets in bins of $Q^2$
XLabel= $x^{obs}_{\gamma}$
YLabel= $d^2\sigma /dQ^2dx^{obs}_{\gamma}$ $[nb$ $GeV^{-2}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[10,$ $100]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.9
END PLOT

BEGIN PLOT /H1_2007_I736052/d30-x01-y01
Title= $d\sigma /dx^{obs}_{g}$ for $D^{*\pm}$ production with dijets
XLabel= $\log_{10}(x^{obs}_{g})$
YLabel= $d\sigma /dx^{obs}_{g}$ $[nb]$
LogX=0
LogY=1
LegendAlign=r
END PLOT

BEGIN PLOT /H1_2007_I736052/d31-x01-y01
Title= $d^2\sigma /dQ^2dx^{obs}_{g}$ for $D^{*\pm}$ production with dijets in bins of $Q^2$
XLabel= $\log_{10}(x^{obs}_{g})$
YLabel= $d^2\sigma /dQ^2dx^{obs}_{g}$ $[nb$ $GeV^{-2}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[2,$ $5]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.3
END PLOT

BEGIN PLOT /H1_2007_I736052/d31-x01-y02
Title= $d^2\sigma /dQ^2dx^{obs}_{g}$ for $D^{*\pm}$ production with dijets in bins of $Q^2$
XLabel= $\log_{10}(x^{obs}_{g})$
YLabel= $d^2\sigma /dQ^2dx^{obs}_{g}$ $[nb$ $GeV^{-2}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[5,$ $10]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.3
END PLOT

BEGIN PLOT /H1_2007_I736052/d31-x01-y03
Title= $d^2\sigma /dQ^2dx^{obs}_{g}$ for $D^{*\pm}$ production with dijets in bins of $Q^2$
XLabel= $\log_{10}(x^{obs}_{g})$
YLabel= $d^2\sigma /dQ^2dx^{obs}_{g}$ $[nb$ $GeV^{-2}]$
LogX=0
LogY=1
Legend=1
CustomLegend= $Q^2\in[10,$ $100]$ $GeV^2$
LegendXPos=0.05
LegendYPos=0.3
END PLOT
