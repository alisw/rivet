BEGIN PLOT /H1_1997_I445116/d*
LegendYPos=0.9
LegendXPos=0.3
END PLOT

BEGIN PLOT /H1_1997_I445116/d01-x01-y01
Title= fragmentation function $, 12 < Q^2 <100$
XLabel=$x_p$
YLabel= $1/N dn/dx_p$
END PLOT

BEGIN PLOT /H1_1997_I445116/d01-x01-y02
Title= fragmentation function $, 100 < Q^2 < 8000$
XLabel=$x_p$
YLabel= $1/N dn/dx_p$
END PLOT

BEGIN PLOT /H1_1997_I445116/d02-x01-y01
LogY=1
Title= fragmentation function $, 12 < Q^2 <100$
XLabel=$\xi$
YLabel= $1/N dn/d\xi$
END PLOT

BEGIN PLOT /H1_1997_I445116/d02-x01-y02
Title= fragmentation function $, 100 < Q^2 < 8000$
XLabel=$\xi$
YLabel= $1/N dn/d\xi$
END PLOT

BEGIN PLOT /H1_1997_I445116/d03-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.02 < x_p < 0.05 $
END PLOT

BEGIN PLOT /H1_1997_I445116/d04-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.05 < x_p < 0.10 $ 
END PLOT

BEGIN PLOT /H1_1997_I445116/d05-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.10 < x_p < 0.20 $ 
END PLOT

BEGIN PLOT /H1_1997_I445116/d06-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.20 < x_p < 0.30$
END PLOT

BEGIN PLOT /H1_1997_I445116/d07-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.30 < x_p < 0.40$
END PLOT

BEGIN PLOT /H1_1997_I445116/d08-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.40 < x_p < 0.50$
END PLOT

BEGIN PLOT /H1_1997_I445116/d09-x01-y01
Title= Current hemisphere fragmentation as a function of $Q$
XLabel=$Q$
YLabel= $1/N dn/dx_p$
CustomLegend = $0.50 < x_p < 0.70$
END PLOT

BEGIN PLOT /H1_1997_I445116/d10-x01-y01
Title= Average Charge Multiplicity
XLabel=$Q$
YLabel= Average Charge Multiplicity
CustomLegend = without the EFS
END PLOT

BEGIN PLOT /H1_1997_I445116/d11-x01-y01
Title= Average Charge Multiplicity
XLabel= $Q$
YLabel= Average Charge Multiplicity
CustomLegend = with the EFS
END PLOT

BEGIN PLOT /H1_1997_I445116/d12-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend =$<Q> = 5.5$ GeV
END PLOT

BEGIN PLOT /H1_1997_I445116/d13-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend = $<Q>=19.6$ GeV
END PLOT

BEGIN PLOT /H1_1997_I445116/d14-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend = $ 12 < Q^2 < 15$  GeV$^2$
END PLOT

BEGIN PLOT /H1_1997_I445116/d15-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend =$ 15 < Q^2 < 20$ GeV$^2$
END PLOT

BEGIN PLOT /H1_1997_I445116/d16-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend = $ 20 < Q^2 < 40$ GeV$^2$
END PLOT

BEGIN PLOT /H1_1997_I445116/d17-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend = $ 40 <Q^2 < 60$ GeV$^2$
END PLOT

BEGIN PLOT /H1_1997_I445116/d18-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend = $ 60 < Q^2 < 80$ GeV$^2$
END PLOT

BEGIN PLOT /H1_1997_I445116/d19-x01-y01
Title= Invariant charged hadron energy spectrum in the current hemisphere
XLabel= $E$
YLabel= $1/N Edn/d^{3}p$ (GeV$^{-2}$)
CustomLegend = $ 80 Q^2 100$ GeV$^2$
END PLOT

BEGIN PLOT /H1_1997_I445116/d20-x01-y01
The probability per event of observing a charged hadronic multiplicity
XLabel= $n$
YLabel= $P(n)$
CustomLegend = $12<Q2<30 , 6\times 10^{-4}<x<2\times 10^{-3}$
END PLOT

BEGIN PLOT /H1_1997_I445116/d21-x01-y01
The probability per event of observing a charged hadronic multiplicity
XLabel= $n$
YLabel= $P(n)$
CustomLegend = $12<Q2<30 , 2\times 10^{-3}<x<  10^{-2}$
END PLOT

BEGIN PLOT /H1_1997_I445116/d22-x01-y01
The probability per event of observing a charged hadronic multiplicity
XLabel= $n$
YLabel= $P(n)$
CustomLegend = $30<Q2<80$,$6 \times10^{-4}<x<2 \times 10^{-3}$
END PLOT

BEGIN PLOT /H1_1997_I445116/d23-x01-y01
The probability per event of observing a charged hadronic multiplicity
XLabel= $n$
YLabel= $P(n)$
CustomLegend = $30<Q2<80 , 2\times 10^{-3}<x<10^{-2}$
END PLOT

BEGIN PLOT /H1_1997_I445116/d24-x01-y01
The probability per event of observing a charged hadronic multiplicity
XLabel= $n$
YLabel= $P(n)$
CustomLegend = $100<Q2<500 , 2\times 10^{-3}<x<10^{-2}$
END PLOT

BEGIN PLOT /H1_1997_I445116/d25-x01-y01
The probability per event of observing a charged hadronic multiplicity
XLabel= $n$
YLabel= $P(n)$
CustomLegend = $100<Q2<500 , 10^{-2}<x<2\times 10^{-1}$
END PLOT

