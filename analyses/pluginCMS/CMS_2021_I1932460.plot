BEGIN PLOT /CMS_2021_I1932460/d01-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=p$_{\rm T,1}$ [GeV]
YLabel=d$\sigma$/dp$_{\rm T,1}$ [pb/GeV]
YMin=100
YMax=3200000
RatioPlotYMin=0.3
RatioPlotYMax=5.5
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d02-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=p$_{\rm T,2}$ [GeV]
YLabel=d$\sigma$/dp$_{\rm T,2}$ [pb/GeV]
YMin=70
YMax=10000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d03-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=p$_{\rm T,3}$ [GeV]
YLabel=d$\sigma$/dp$_{\rm T,3}$ [pb/GeV]
YMin=12
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d04-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=p$_{\rm T,4}$ [GeV]
YLabel=d$\sigma$/dp$_{\rm T,4}$ [pb/GeV]
YMin=10
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d05-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\eta_{1}$
YLabel=d$\sigma$/d$\eta_{1}$ [pb]
YMin=20000
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d06-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\eta_{2}$
YLabel=d$\sigma$/d$\eta_{2}$ [pb]
YMin=20000
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d07-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\eta_{3}$
YLabel=d$\sigma$/d$\eta_{3}$ [pb]
YMin=20000
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d08-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\eta_{4}$
YLabel=d$\sigma$/d$\eta_{4}$ [pb]
YMin=20000
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.8
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d09-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta\phi_{\rm Soft}$ [rad]
YLabel=dN/d$\Delta\phi_{\rm Soft}$ [a.u.]
YMin=0.2
YMax=30
RatioPlotYMin=0.82
RatioPlotYMax=1.32
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d10-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta\phi_{\rm 3j}^{\rm min}$ [rad]
YLabel=dN/d$\Delta\phi_{\rm 3j}^{\rm min}$ [a.u.]
YMin=0.03
YMax=220
RatioPlotYMin=0.5
RatioPlotYMax=1.72
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d11-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta$Y
YLabel=dN/d$\Delta$Y [a.u.]
YMin=0.009
YMax=35
RatioPlotYMin=0.4
RatioPlotYMax=2.9
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d12-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\phi_{\rm ij}$ [rad]
YLabel=dN/d$\phi_{\rm ij}$ [a.u.]
YMin=0.04
YMax=20
RatioPlotYMin=0.82
RatioPlotYMax=1.95
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d13-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta{\rm p}_{\rm T,Soft}$
YLabel=dN/d$\Delta{\rm p}_{\rm T,Soft}$ [a.u.]
YMin=0.029
YMax=20
RatioPlotYMin=0.82
RatioPlotYMax=1.32
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d14-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta$S [rad]
YLabel=dN/d$\Delta$S [a.u.]
YMin=0.004
YMax=20
RatioPlotYMin=0.5
RatioPlotYMax=1.5
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d45-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta\phi_{\rm Soft}$ [rad]
YLabel=d$\sigma$/d$\Delta\phi_{\rm Soft}$ [pb/rad]
YMin=300000
YMax=12000000
RatioPlotYMin=0.3
RatioPlotYMax=3.2
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d46-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta\phi_{\rm 3j}^{\rm min}$ [rad]
YLabel=d$\sigma$/d$\Delta\phi_{\rm 3j}^{\rm min}$ [pb/rad]
YMin=8500
YMax=150000000
RatioPlotYMin=0.5
RatioPlotYMax=3.2
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d47-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta$Y
YLabel=d$\sigma$/d$\Delta$Y [pb]
YMin=5000
YMax=21000000
RatioPlotYMin=0.3
RatioPlotYMax=4.9
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d48-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\phi_{\rm ij}$ [rad]
YLabel=d$\sigma$/d$\phi_{\rm ij}$ [pb/rad]
YMin=200000
YMax=15000000
RatioPlotYMin=0.3
RatioPlotYMax=4.5
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d49-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta{\rm p}_{\rm T,Soft}$
YLabel=d$\sigma$/d$\Delta{\rm p}_{\rm T,Soft}$ [pb]
YMin=800000
YMax=150000000
RatioPlotYMin=0.3
RatioPlotYMax=3.2
END PLOT

BEGIN PLOT /CMS_2021_I1932460/d50-x01-y01
Title=CMS, 13 TeV, 4-jet production
XLabel=$\Delta$S [rad]
YLabel=d$\sigma$/d$\Delta$S [pb/rad]
YMin=10000
YMax=80000000
RatioPlotYMin=0.1
RatioPlotYMax=3.2
END PLOT
