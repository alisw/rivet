# BEGIN PLOT /CMS_2021_I1920187/d02-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d04-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d06-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d08-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d10-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d12-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d14-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d16-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d18-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d155-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d156-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d157-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d158-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d159-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d160-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d161-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d162-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d163-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d164-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d165-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d179-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d180-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d181-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d182-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d183-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d184-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d185-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d186-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d187-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d188-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d189-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d190-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d191-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d209-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d210-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d211-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d212-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d213-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d214-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d215-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d216-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d217-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d218-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d219-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d220-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d234-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d235-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d236-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d237-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d238-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d239-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d240-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d241-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d242-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d243-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d244-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d245-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d246-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d265-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d266-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d267-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d268-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d269-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d270-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d271-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d272-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d273-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d274-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d275-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d276-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d277-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d291-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d292-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d293-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d294-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d295-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d296-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d297-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d298-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d299-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d300-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d301-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d302-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d303-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d322-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d323-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d324-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d325-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d326-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d327-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d328-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d329-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d330-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d331-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d332-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d333-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d334-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d348-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d349-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d350-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d351-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d352-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d353-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d354-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d355-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d356-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d357-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d358-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d359-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d360-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d379-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d380-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d381-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d382-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d383-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d384-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d385-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d386-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d387-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d388-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d389-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d390-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d391-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d405-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d406-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d407-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d408-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d409-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d410-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d411-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d412-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d413-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d414-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d415-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d416-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d417-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d436-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d437-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d438-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d439-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d440-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d441-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d442-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d443-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d444-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d445-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d446-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d447-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d448-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d462-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d463-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d464-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d465-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d466-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d467-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d468-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d469-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d470-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d471-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d472-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d473-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d474-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d492-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d493-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d494-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d495-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d496-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d497-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d498-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d499-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d500-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d501-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d502-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d503-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d517-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d518-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d519-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d520-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d521-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d522-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d523-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d524-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d525-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d526-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d527-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d528-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d529-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d547-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d548-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d549-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d550-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d551-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d552-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d553-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d554-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d555-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d556-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d557-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d558-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d572-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d573-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d574-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d575-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d576-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d577-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d578-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d579-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d580-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d581-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d582-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d583-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d584-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d602-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d603-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d604-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d605-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d606-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d607-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d608-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d609-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d610-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d611-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d612-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d613-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d627-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d628-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d629-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d630-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d631-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d632-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d633-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d634-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d635-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d636-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d637-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d638-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d639-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d657-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d658-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d659-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d660-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d661-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d662-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d663-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d664-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d665-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d666-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d667-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d668-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d682-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d683-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d684-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d685-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d686-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d687-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d688-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d689-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d690-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d691-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d692-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d693-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d694-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d712-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d713-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d714-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d715-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d716-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d717-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d718-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d719-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d720-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d721-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d722-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d723-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d737-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d738-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d739-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d740-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d741-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d742-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d743-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d744-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d745-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d746-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d747-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d748-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d749-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d768-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d769-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d770-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d771-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d772-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d773-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d774-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d775-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d776-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d777-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d778-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d779-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d780-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d794-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d795-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d796-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d797-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d798-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d799-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d800-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d801-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d802-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d803-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d804-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d805-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d806-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d825-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d826-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d827-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d828-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d829-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d830-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d831-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d832-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d833-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d834-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d835-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d836-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d837-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d851-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d852-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d853-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d854-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d855-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d856-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d857-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d858-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d859-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d860-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d861-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d862-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d863-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d882-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d883-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d884-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d885-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d886-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d887-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d888-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d889-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d890-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d891-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d892-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d893-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d894-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d908-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d909-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d910-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d911-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d912-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d913-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d914-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d915-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d916-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d917-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d918-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d919-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d920-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d939-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d940-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d941-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d942-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d943-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d944-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d945-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d946-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d947-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d948-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d949-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d950-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d951-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d965-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d966-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d967-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d968-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d969-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d970-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d971-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d972-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d973-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d974-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d975-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d976-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d977-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d996-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d997-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d998-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d999-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1000-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1001-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1002-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1003-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1004-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1005-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1006-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1007-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1008-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1022-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1023-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1024-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1025-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1026-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1027-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1028-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1029-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1030-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1031-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1032-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1033-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1034-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1053-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1054-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1055-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1056-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1057-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1058-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1059-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1060-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1061-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1062-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1063-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1064-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1065-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1079-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1080-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1081-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1082-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1083-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1084-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1085-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1086-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1087-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1088-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1089-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1090-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1091-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1110-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1111-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1112-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1113-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1114-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1115-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1116-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1117-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1118-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1119-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1120-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1121-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1122-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1136-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1137-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1138-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1139-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1140-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1141-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1142-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1143-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1144-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1145-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1146-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1147-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1148-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1167-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1168-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1169-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1170-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1171-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1172-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1173-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1174-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1175-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1176-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1177-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1178-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1179-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1193-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1194-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1195-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1196-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1197-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1198-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1199-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1200-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1201-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1202-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1203-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1204-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1205-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1224-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1225-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1226-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1227-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1228-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1229-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1230-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1231-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1232-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1233-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1234-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1235-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1236-x01-y01
Title=CMS, 13 TeV, AK4 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1250-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1251-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1252-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1253-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1254-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1255-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1256-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1257-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1258-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1259-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1260-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1261-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1262-x01-y01
Title=CMS, 13 TeV, AK4 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1271-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1272-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1273-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1274-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1275-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1276-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1277-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1278-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1279-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1280-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1281-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1282-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1283-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1284-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1285-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1286-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1287-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1288-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1289-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1290-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1291-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1292-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1293-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1294-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1295-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1314-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1315-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1316-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1317-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1318-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1319-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1320-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1321-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1322-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1323-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1324-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1325-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1326-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1327-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1328-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1329-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1330-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1331-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1332-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1333-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1334-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1335-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1336-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1337-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1338-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1339-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1358-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1359-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1360-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1361-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1362-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1363-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1364-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1365-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1366-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1367-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1368-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1369-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1370-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1371-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1372-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1373-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1374-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1375-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1376-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1377-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1378-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1379-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1380-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1381-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1382-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1383-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1402-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1403-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1404-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1405-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1406-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1407-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1408-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1409-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1410-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1411-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1412-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1413-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1414-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1415-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1416-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1417-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1418-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1419-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1420-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1421-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1422-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1423-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1424-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1425-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1426-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1427-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1446-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1447-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1448-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1449-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1450-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1451-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1452-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1453-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1454-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1455-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1456-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1457-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1458-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1459-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1460-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1461-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1462-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1463-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1464-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1465-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1466-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1467-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1468-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1469-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1470-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1471-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1490-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1491-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1492-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1493-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1494-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1495-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1496-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1497-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1498-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1499-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1500-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1501-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1502-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1503-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1504-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1505-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1506-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1507-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1508-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1509-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1510-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1511-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1512-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1513-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1514-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1515-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1525-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1526-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1527-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1528-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1529-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1530-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1531-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1532-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1533-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1534-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1535-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1536-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1537-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1538-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1539-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1540-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1541-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1542-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1543-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1544-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1545-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1546-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1547-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1548-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1549-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1550-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1560-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1561-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1562-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1563-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1564-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1565-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1566-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1567-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1568-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1569-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1570-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1571-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1572-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1573-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1574-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1575-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1576-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1577-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1578-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1579-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1580-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1581-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1582-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1583-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1584-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1585-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1595-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1596-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1597-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1598-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1599-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1600-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1601-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1602-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1603-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1604-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1605-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1606-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1607-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1608-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1609-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1610-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1611-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1612-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1613-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1614-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1615-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1616-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1617-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1618-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1619-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1620-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1630-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1631-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1632-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1633-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1634-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1635-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1636-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1637-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1638-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1639-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1640-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1641-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1642-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1643-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1644-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1645-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1646-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1647-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1648-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1649-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1650-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1651-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1652-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1653-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1654-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1655-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1674-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1675-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1676-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1677-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1678-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1679-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1680-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1681-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1682-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1683-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1684-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1685-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1686-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1687-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1688-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1689-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1690-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1691-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1692-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1693-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1694-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1695-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1696-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1697-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1698-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1699-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1718-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1719-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1720-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1721-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1722-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1723-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1724-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1725-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1726-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1727-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1728-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1729-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1730-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1731-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1732-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1733-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1734-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1735-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1736-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1737-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1738-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1739-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1740-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1741-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1742-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1743-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1762-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1763-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1764-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1765-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1766-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1767-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1768-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1769-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1770-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1771-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1772-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1773-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1774-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1775-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1776-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1777-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1778-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1779-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1780-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1781-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1782-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1783-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1784-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1785-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1786-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1787-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1806-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1807-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1808-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1809-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1810-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1811-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1812-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1813-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1814-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1815-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1816-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1817-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1818-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1819-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1820-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1821-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1822-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1823-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1824-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1825-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1826-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1827-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1828-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1829-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1830-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1831-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1850-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1851-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1852-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1853-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1854-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1855-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1856-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1857-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1858-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1859-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1860-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1861-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1862-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1863-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1864-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1865-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1866-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1867-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1868-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1869-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1870-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1871-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1872-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1873-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1874-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1875-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1894-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1895-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1896-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1897-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1898-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1899-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1900-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1901-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1902-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1903-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1904-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1905-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1906-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1907-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1908-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1909-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1910-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1911-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1912-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1913-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1914-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1915-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1916-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1917-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1918-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1919-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1938-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1939-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1940-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1941-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1942-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1943-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1944-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1945-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1946-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1947-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1948-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1949-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1950-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1951-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1952-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1953-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1954-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1955-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1956-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1957-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1958-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1959-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1960-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1961-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1962-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1963-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1982-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1983-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1984-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1985-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1986-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1987-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1988-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1989-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1990-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1991-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1992-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1993-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1994-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1995-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1996-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1997-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1998-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1999-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2000-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2001-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2002-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2003-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2004-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2005-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2006-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2007-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2026-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2027-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2028-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2029-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2030-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2031-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2032-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2033-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2034-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2035-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2036-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2037-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2038-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2039-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2040-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2041-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2042-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2043-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2044-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2045-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2046-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2047-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2048-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2049-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2050-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2051-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2070-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2071-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2072-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2073-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2074-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2075-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2076-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2077-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2078-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2079-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2080-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2081-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2082-x01-y01
Title=CMS, 13 TeV, AK8 jets, central dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2083-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2084-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2085-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2086-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2087-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2088-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2089-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2090-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2091-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 408 $<p_{T}^{\text{jet}}<$ 481 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2092-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 481 $<p_{T}^{\text{jet}}<$ 614 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2093-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 614 $<p_{T}^{\text{jet}}<$ 800 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2094-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 800 $<p_{T}^{\text{jet}}<$ 1000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2095-x01-y01
Title=CMS, 13 TeV, AK8 jets, forward dijet region, 1000 $<p_{T}^{\text{jet}}<$ 4000 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT

# BEGIN PLOT /CMS_2021_I1920187/d01-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d03-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d05-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d07-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d09-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d11-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d13-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d15-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d17-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d139-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d140-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d141-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d142-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d143-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d144-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d145-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d192-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d193-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d194-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d195-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d196-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d197-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d198-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d199-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d247-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d248-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d249-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d250-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d251-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d252-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d253-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d254-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d255-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d304-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d305-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d306-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d307-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d308-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d309-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d310-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d311-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d312-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d361-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d362-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d363-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d364-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d365-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d366-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d367-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d368-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d369-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d418-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d419-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d420-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d421-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d422-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d423-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d424-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d425-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d426-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d475-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d476-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d477-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d478-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d479-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d480-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d481-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d482-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d530-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d531-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d532-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d533-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d534-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d535-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d536-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d537-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d585-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d586-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d587-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d588-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d589-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d590-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d591-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d592-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d640-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d641-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d642-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d643-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d644-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d645-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d646-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d647-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d695-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d696-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d697-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d698-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d699-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d700-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d701-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d702-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d750-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d751-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d752-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d753-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d754-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d755-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d756-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d757-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d758-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d807-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d808-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d809-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d810-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d811-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d812-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d813-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d814-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d815-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d864-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d865-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d866-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d867-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d868-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d869-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d870-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d871-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d872-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d921-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d922-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d923-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d924-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d925-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d926-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d927-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d928-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d929-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d978-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d979-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d980-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d981-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d982-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d983-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d984-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d985-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d986-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1035-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1036-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1037-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1038-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1039-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1040-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1041-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1042-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1043-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1092-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1093-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1094-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1095-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1096-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1097-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1098-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1099-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1100-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1149-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1150-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1151-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1152-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1153-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1154-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1155-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1156-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1157-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1206-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1207-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1208-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1209-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1210-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1211-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1212-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1213-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1214-x01-y01
Title=CMS, 13 TeV, AK4 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1263-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1264-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1265-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1266-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1267-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1268-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1269-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1270-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1296-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1297-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1298-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1299-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1300-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1301-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1302-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1303-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1304-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1340-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1341-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1342-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1343-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1344-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1345-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1346-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1347-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1348-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1384-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1385-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1386-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1387-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1388-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1389-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1390-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1391-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1392-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1428-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1429-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1430-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1431-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1432-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1433-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1434-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1435-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1436-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1472-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1473-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1474-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1475-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1476-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1477-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1478-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1479-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1480-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1516-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1517-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1518-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1519-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1520-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1521-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1522-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1523-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1524-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1551-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1552-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1553-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1554-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1555-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1556-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1557-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1558-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1559-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1586-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1587-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1588-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1589-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1590-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1591-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1592-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1593-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1594-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1621-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1622-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1623-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1624-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1625-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1626-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1627-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1628-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1629-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1656-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1657-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1658-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1659-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1660-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1661-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1662-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1663-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1664-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1700-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1701-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1702-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1703-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1704-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1705-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1706-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1707-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1708-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed LHA (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1744-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1745-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1746-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1747-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1748-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1749-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1750-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1751-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1752-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1788-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1789-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1790-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1791-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1792-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1793-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1794-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1795-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1796-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2 (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1832-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1833-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1834-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1835-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1836-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1837-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1838-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1839-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1840-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1876-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1877-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1878-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1879-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1880-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1881-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1882-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1883-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1884-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width (charged-only)
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1920-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1921-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1922-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1923-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1924-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1925-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1926-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1927-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1928-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed multiplicity
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=150.
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1964-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1965-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1966-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1967-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1968-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1969-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1970-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1971-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d1972-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed pTD2
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2008-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2009-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2010-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2011-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2012-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2013-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2014-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2015-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2016-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed thrust
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2052-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 50 $<p_{T}^{\text{jet}}<$ 65 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2053-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 65 $<p_{T}^{\text{jet}}<$ 88 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2054-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 88 $<p_{T}^{\text{jet}}<$ 120 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2055-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 120 $<p_{T}^{\text{jet}}<$ 150 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2056-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 150 $<p_{T}^{\text{jet}}<$ 186 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2057-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 186 $<p_{T}^{\text{jet}}<$ 254 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2058-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 254 $<p_{T}^{\text{jet}}<$ 326 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2059-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 326 $<p_{T}^{\text{jet}}<$ 408 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT


# BEGIN PLOT /CMS_2021_I1920187/d2060-x01-y01
Title=CMS, 13 TeV, AK8 jets, Z+jet region, 408 $<p_{T}^{\text{jet}}<$ 1500 GeV
XLabel=groomed width
YLabel=$\frac{1}{\mathrm d N / \mathrm d p_{T}}\frac{\mathrm d^{2}N}{\mathrm d p_{T}~\mathrm d \lambda}$
LeftMargin=1.7
XMin=0.0
XMax=1.0
LogY=0
XTwosidedTicks=1
YTwosidedTicks=1
NormalizeToIntegral=1
LegendXPos=0.5
# END PLOT
