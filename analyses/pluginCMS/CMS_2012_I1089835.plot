# BEGIN PLOT /CMS_2012_I1089835/d04-x01-y01
Title=CMS, 7 TeV, inclusive b-jet, $|y| < 0.5$
XLabel=$p_T (bjet)$ [GeV]
YLabel=$\frac{d\sigma}{dp_T \,dy}$ [pb / GeV]
RatioPlot=1
LogX=1
RatioPlotYMax=2
# END PLOT

# BEGIN PLOT /CMS_2012_I1089835/d05-x01-y01
Title=CMS, 7 TeV, inclusive b-jet, $0.5 < |y| < 1.0$
XLabel=$p_T (bjet)$ [GeV]
YLabel=$\frac{d\sigma}{dp_T \,dy}$ [pb / GeV]
RatioPlot=1
LogX=1
RatioPlotYMax=2
# END PLOT

# BEGIN PLOT /CMS_2012_I1089835/d06-x01-y01
Title=CMS, 7 TeV, inclusive b-jet, $1.0<|y| < 1.5$
XLabel=$p_T (bjet)$ [GeV]
YLabel=$\frac{d\sigma}{dp_T \,dy}$ [pb / GeV]
RatioPlot=1
LogX=1
RatioPlotYMax=2
# END PLOT

# BEGIN PLOT /CMS_2012_I1089835/d07-x01-y01
Title=CMS, 7 TeV, inclusive b-jet, $1.5<|y| < 2.0$
XLabel=$p_T (bjet)$ [GeV]
YLabel=$\frac{d\sigma}{dp_T \,dy}$ [pb / GeV]
RatioPlot=1
LogX=1
RatioPlotYMax=2
# END PLOT

# BEGIN PLOT /CMS_2012_I1089835/d08-x01-y01
Title=CMS, 7 TeV, inclusive b-jet, $2.0 < |y| < 2.2$
XLabel=$p_T (bjet)$ [GeV]
YLabel=$\frac{d\sigma}{dp_T \,dy}$ [pb / GeV]
RatioPlot=1
LogX=1
RatioPlotYMax=2
# END PLOT