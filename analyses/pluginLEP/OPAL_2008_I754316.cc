// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/GammaGammaFinalState.hh"
#include "Rivet/Projections/FastJets.hh"

namespace Rivet {


  /// @brief Jet production in photon-photon collisions at 206 GeV
  class OPAL_2008_I754316 : public Analysis {
  public:

    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(OPAL_2008_I754316);


    /// @name Analysis methods
    /// @{

    /// Book histograms and initialise projections before the run
    void init() {
      // get the hadronic final state
      const FinalState & fs = declare(GammaGammaFinalState(), "FS");
      declare(FastJets(fs, JetAlg::KT,1.),"Jets");

      // Book histograms
      book(_h_y1,1, 1, 1);
      book(_h_y2,2, 1, 1);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      Jets jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 5*GeV and Cuts::abseta < 1.5);
      if(jets.empty()) vetoEvent;
      for(const Jet & jet : jets) {
      	_h_y2->fill(jet.pT());
      	if(abs(jet.eta())<1.0)
      	  _h_y1->fill(jet.pT());
      }
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      scale(_h_y1, crossSection()/picobarn/sumOfWeights());
      scale(_h_y2, crossSection()/picobarn/sumOfWeights());

    }

    /// @}


    /// @name Histograms
    /// @{
    Histo1DPtr _h_y1, _h_y2;
    /// @}


  };


  RIVET_DECLARE_PLUGIN(OPAL_2008_I754316);


}
