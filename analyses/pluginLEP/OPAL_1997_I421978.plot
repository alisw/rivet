# BEGIN PLOT /OPAL_1997_I421978/d01-x01-y01
Title=$\Lambda^0$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d02-x01-y01
Title=$\Lambda^0$ scaled momentum
XLabel=$\ln(1/x_p)$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\ln(1/x_p)$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d03-x01-y01
Title=$\Xi^-$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d04-x01-y01
Title=$\Xi^-$ scaled momentum
XLabel=$\ln(1/x_p)$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\ln(1/x_p)$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d05-x01-y01
Title=$\Sigma^+(1385)$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d06-x01-y01
Title=$\Sigma^+(1385)$ scaled momentum
XLabel=$\ln(1/x_p)$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\ln(1/x_p)$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d07-x01-y01
Title=$\Sigma^-(1385)$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d08-x01-y01
Title=$\Sigma^-(1385)$ scaled momentum
XLabel=$\ln(1/x_p)$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\ln(1/x_p)$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d09-x01-y01
Title=$\Xi^0(1530)$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d10-x01-y01
Title=$\Xi^0(1530)$ scaled momentum
XLabel=$\ln(1/x_p)$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\ln(1/x_p)$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d11-x01-y01
Title=$\Lambda^0(1520)$ scaled momentum
XLabel=$x_p$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_p$
# END PLOT

# BEGIN PLOT /OPAL_1997_I421978/d12-x01-y01
Title=$\Lambda^0(1520)$ scaled momentum
XLabel=$\ln(1/x_p)$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}\ln(1/x_p)$
# END PLOT
