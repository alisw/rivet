BEGIN PLOT /ALEPH_1991_I319520/d01-x01-y01
Title=Total charged multiplicity
XLabel=$n_\mathrm{ch}$
YLabel=$2/\sigma \, \mathrm{d}{\sigma}/\mathrm{d}{n_\mathrm{ch}}$
LegendXPos=0.25
LegendYPos=0.7
END PLOT

BEGIN PLOT /ALEPH_1991_I319520/d02-x01-y01
Title=Mean charged multiplicity
LogY=0
END PLOT
