# BEGIN PLOT /ATLAS_2016_I1494075/*
XTwosidedTicks=1
YTwosidedTicks=1
LegendAlign=r
LogY=0
YMin=0
RatioPlotMode=datamc
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d02-x01-y01
XLabel=$\mathrm{p}_\mathrm{T}^{\mathrm{Z}_\mathrm{lead}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} \mathrm{p}_\mathrm{T}^{\mathrm{Z}_\mathrm{lead}}$ [fb/GeV]
Title=Measured differential cross section normalised to $\mathrm{p}_\mathrm{T}^{\mathrm{Z}_\mathrm{lead}}$
YMax=140
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d03-x01-y01
XLabel=$\mathrm{N}_\mathrm{jets}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \mathrm{N}_\mathrm{jets}$ [fb/$\mathrm{N}_\mathrm{jets}$]
Title=Measured differential cross section normalised to $\mathrm{N}_\mathrm{jets}$
YMax=10000
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d04-x01-y01
XLabel=$\Delta \phi (\mathrm{l}^{+},\mathrm{l}^{-})_{\mathrm{lead}}$ [rad]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\Delta \phi (l^{+},l^{-})_{lead}$ [pb/rad]
Title=Measured differential cross section normalised to $\Delta \phi (l^{+},l^{-})_{lead}$
YMax=9
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d05-x01-y01
XLabel=$\Delta \mathrm{y(Z,Z)}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \Delta \mathrm{y(Z,Z)}$ [pb]
Title=Measured differential cross section normalised to $\Delta \mathrm{y(Z,Z)}$ 
YMax=10
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d06-x01-y01
XLabel=$\mathrm{p}_\mathrm{T}^\mathrm{Z}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\mathrm{p}_\mathrm{T}^\mathrm{Z}$ [fb/GeV]
Title=Measured differential cross section normalised to $\mathrm{p}_\mathrm{T}^\mathrm{Z}$ 
YMax=0.3
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d07-x01-y01
XLabel=$\Delta \phi (\mathrm{l}^{+},\mathrm{l}^{-})$ [rad]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\Delta \phi (\mathrm{l}^{+},\mathrm{l}^{-})$ [fb/rad]
Title=Measured differential cross section normalised to $\Delta \phi (\mathrm{l}^{+},\mathrm{l}^{-})$ 
YMax=23
# END PLOT

# BEGIN PLOT /ATLAS_2016_I1494075/d08-x01-y01
XLabel=$\mathrm{m}_{\mathrm{T}}^{\mathrm{ZZ}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}\mathrm{m}_{\mathrm{T}}^{\mathrm{ZZ}}$ [fb/GeV]
Title=Measured differential cross section normalised to $\mathrm{m}_{\mathrm{T}}^{\mathrm{ZZ}}$ 
YMax=0.17
# END PLOT
