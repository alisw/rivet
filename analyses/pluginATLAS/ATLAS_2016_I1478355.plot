BEGIN PLOT /ATLAS_2016_I1478355/d01-x01-y01
Title= $m_{b\bar{b}}$ distribution
XLabel=$m_{b\bar{b}}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}m_{b\bar{b}}$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ATLAS_2016_I1478355/d02-x01-y01
Title= $\Delta\phi(b,\bar{b})$ distribution
XLabel=$\Delta\phi(b,\bar{b})$ [rad]
YLabel=$\mathrm{d}\sigma/\mathrm{d}\Delta\phi(b,\bar{b})$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ATLAS_2016_I1478355/d03-x01-y01
Title= Rapidity difference distribution
XLabel=y*
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{y*}$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ATLAS_2016_I1478355/d04-x01-y01
Title= $\Delta \, R(b,\bar{b})$ distribution
XLabel=$\Delta \, R(b,\bar{b})$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\Delta \, R(b,\bar{b})$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ATLAS_2016_I1478355/d05-x01-y01
Title= $p_{T,b\bar{b}}$ distribution
XLabel=$p_{T,b\bar{b}}$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_{T,b\bar{b}}$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /ATLAS_2016_I1478355/d06-x01-y01
Title= Rapidity boost distribution
XLabel=$\mathrm{y}_{B}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\mathrm{y}_{B}$ [pb/GeV]
# + any additional plot settings you might like, see make-plots documentation
END PLOT
