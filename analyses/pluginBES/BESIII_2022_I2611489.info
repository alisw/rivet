Name: BESIII_2022_I2611489
Year: 2022
Summary: Positron momentum spectrum in semileptonic $\Lambda_c^+$ decay
Experiment: BESIII
Collider: BEPC
InspireID: 2611489
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2212.03753 [hep-ex]
RunInfo: e+e -> hadrons
Beams: [e+, e-]
Energies: [4,6]
Description:
'Measurement of the positron momentum spectrum in the lab frame for
 semileptonic $\Lambda_c^+$ decay.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2018mug
BibTeX: '@article{BESIII:2018mug,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Measurement of the absolute branching fraction of the inclusive semileptonic $\Lambda_c^+$ decay}",
    eprint = "1805.09060",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevLett.121.251801",
    journal = "Phys. Rev. Lett.",
    volume = "121",
    number = "25",
    pages = "251801",
    year = "2018"
}
'
