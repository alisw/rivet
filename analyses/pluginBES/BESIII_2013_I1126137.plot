BEGIN PLOT /BESIII_2013_I1126137/d01-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$\text{d}\mathcal{B}(J/\psi\to3\gamma)/\text{d}x_\gamma$
LogY=0
END PLOT
