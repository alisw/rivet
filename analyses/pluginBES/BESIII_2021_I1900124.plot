BEGIN PLOT /BESIII_2021_I1900124/d01-x01-y01
Title=$\sigma(e^+e^-\to \Lambda\bar{\Lambda})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Lambda\bar{\Lambda})$/fb
ConnectGaps=1
END PLOT
