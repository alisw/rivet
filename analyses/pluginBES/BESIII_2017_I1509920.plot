BEGIN PLOT /BESIII_2017_I1509920/d01
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{e^+e^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d02
XLabel=$\cos\theta_{e^+e^-}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{e^+e^-}$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2017_I1509920/d01-x01-y01
Title=$e^+e^-$ mass distribution in $\psi(2S)\to \chi_{c1} e^+e^-$ 
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d01-x01-y02
Title=$e^+e^-$ mass distribution in $\psi(2S)\to \chi_{c2} e^+e^-$ 
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d01-x01-y03
Title=$e^+e^-$ mass distribution in $\chi_{c1} \to J/\psi e^+e^-$ 
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d01-x01-y04
Title=$e^+e^-$ mass distribution in $\chi_{c2} \to J/\psi e^+e^-$ 
END PLOT

BEGIN PLOT /BESIII_2017_I1509920/d02-x01-y01
Title=Helicity angle in $\psi(2S)\to \chi_{c1} e^+e^-$ 
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d02-x01-y02
Title=Helicity angle in $\psi(2S)\to \chi_{c2} e^+e^-$ 
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d02-x01-y03
Title=Helicity angle in $\chi_{c1} \to J/\psi e^+e^-$ 
END PLOT
BEGIN PLOT /BESIII_2017_I1509920/d02-x01-y04
Title=Helicity angle in $\chi_{c2} \to J/\psi e^+e^-$ 
END PLOT
