BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y01
Title=$K^+\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($m^2_{\psi(2S)\pi^-}<19\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y02
Title=$K^+\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($19<m^2_{\psi(2S)\pi^-}<20.5\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y03
Title=$K^+\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($m^2_{\psi(2S)\pi^-}>20.5\,\text{GeV}^2$)
XLabel=$m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{K^+\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y04
Title=$\psi(2S)\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($m_{K^+\pi^-}>0.796\,$GeV)
XLabel=$m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y05
Title=$\psi(2S)\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($0.796<m_{K^+\pi^-}<0.996\,$GeV)
XLabel=$m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y06
Title=$\psi(2S)\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($0.996<m_{K^+\pi^-}<1.332\,$GeV)
XLabel=$m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y07
Title=$\psi(2S)\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($1.332<m_{K^+\pi^-}<1.532\,$GeV)
XLabel=$m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d01-x01-y08
Title=$\psi(2S)\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($m_{K^+\pi^-}>1.532\,$GeV)
XLabel=$m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2013_I1239347/d02-x01-y01
Title=$\psi(2S)\pi^-$ mass in $B^0\to \psi(2S)K^+\pi^-$ ($K^*$ veto)
XLabel=$m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\psi(2S)\pi^-}$ [$\text{GeV}^2$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2013_I1239347/d03-x01-y01
Title=$\psi(2S)$ helicity angle in $B^0\to \psi(2S)K^+\pi^-$
XLabel=$\cos\theta_{\psi(2S)}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\psi(2S)}$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1239347/d03-x01-y02
Title=Angular between decay planes in $B^0\to \psi(2S)K^+\pi^-$
XLabel=$\phi_{\psi(2S)K^*}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_{\psi(2S)K^*}$
LogY=0
END PLOT
