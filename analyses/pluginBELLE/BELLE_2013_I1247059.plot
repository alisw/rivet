BEGIN PLOT /BELLE_2013_I1247059/d01-x01-y01
Title=$K^\pm\pi^\mp$ mass distribution in $B^0\to\phi K^*$ decays
XLabel=$m_{K^\pm\pi^\mp}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^\pm\pi^\mp}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247059/d01-x01-y02
Title=$\cos\theta_1$ distribution in $B^0\to\phi K^*$ decays
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_1$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247059/d01-x01-y03
Title=$\cos\theta_2$ distribution in $B^0\to\phi K^*$ decays
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_2$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2013_I1247059/d01-x01-y04
Title=$\phi$ distribution in $B^0\to\phi K^*$ decays
XLabel=$\phi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi$
LogY=0
END PLOT
