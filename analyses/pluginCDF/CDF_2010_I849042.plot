# BEGIN PLOT /CDF_2010_I849042/d
XLabel=$p_T(Z)$ / GeV
LogY=0
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d0[12]
YLabel=$\langle N_\mathrm{ch} \rangle / \mathrm{d}\eta\,\mathrm{d}\phi$
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d01-x01-y01
Title=Toward region charged particle density
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d01-x01-y02
Title=Transverse region charged particle density
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d01-x01-y03
Title=Away region charged particle density
LegendXPos=0.10
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d02-x01-y01
Title=TransMAX region charged particle density
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d02-x01-y02
Title=TransMIN region charged particle density
LegendXPos=0.10
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d02-x01-y03
Title=TransDIF region charged particle density
LegendXPos=0.10
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d0[34]
YLabel=$\langle \sum p_T^\mathrm{track} \rangle / \mathrm{d}\eta\,\mathrm{d}\phi$ / GeV
LegendXPos=0.10
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d03-x01-y01
Title=Toward region charged $p_\perp^\mathrm{sum}$ density
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d03-x01-y02
Title=Transverse region charged $p_\perp^\mathrm{sum}$ density
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d03-x01-y03
Title=Away region charged $p_\perp^\mathrm{sum}$ density
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d04-x01-y01
Title=TransMAX region charged $p_\perp^\mathrm{sum}$ density
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d04-x01-y02
Title=TransMIN region charged $p_\perp^\mathrm{sum}$ density
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d04-x01-y03
Title=TransDIF region charged $p_\perp^\mathrm{sum}$ density
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d05
YLabel=$\langle p_T^\mathrm{track} \rangle$ / GeV
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d05-x01-y01
Title=Toward region charged $p_\perp$ average
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d05-x01-y02
Title=Transverse region charged $p_\perp$ average
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d06
YLabel=max $p_T^\mathrm{track}$ / GeV
LegendXPos=0.10
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d06-x01-y01
Title=Toward region charged $p_\perp$ maximum
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d06-x01-y02
Title=Transverse region charged $p_\perp$ maximum
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d0[789]
XLabel=$N_\mathrm{ch}$
LegendXPos=0.10
ShowZero=0
YLabel=$\langle p_T \rangle$ / GeV
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d07-x01-y01
Title=Average lepton-pair $p_\perp$ versus charged multiplicity
YLabel=$\langle p_T(Z) \rangle$ / GeV
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d08-x01-y01
Title=Average charged $p_\perp$ vs charged multiplicity
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d09-x01-y01
Title=Average charged $p_\perp$ vs charged multiplicity, $p_\perp(Z) < 10\,\mathrm{GeV}$
# END PLOT




# BEGIN PLOT /CDF_2010_I849042/d1[01]
YLabel=$\langle N_\mathrm{ch} \rangle / \mathrm{d}\eta\,\mathrm{d}\phi$
LegendYPos=0.5
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d10-x01-y01
Title=Toward region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d10-x01-y02
Title=Transverse region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d10-x01-y03
Title=Away region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d11-x01-y01
Title=TransMAX region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d11-x01-y02
Title=TransMIN region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d11-x01-y03
Title=TransDIF region charged particle density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d1[23]
YLabel=$\langle \sum p_T^\mathrm{track} \rangle / \mathrm{d}\eta\,\mathrm{d}\phi$ / GeV
LegendXPos=0.10
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d12-x01-y01
Title=Toward region charged $\sum p_\perp$ density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d12-x01-y02
Title=Transverse region charged $\sum p_\perp$ density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d12-x01-y03
Title=Away region charged $\sum p_\perp$ density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d13-x01-y01
Title=TransMAX region charged $\sum p_\perp$ density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d13-x01-y02
Title=TransMIN region charged $\sum p_\perp$ density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d13-x01-y03
Title=TransDIF region charged $\sum p_\perp$ density
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT



# BEGIN PLOT /CDF_2010_I849042/d1[45]
LegendXPos=0.10
ShowZero=0
YLabel=$\langle p_T^\mathrm{track} \rangle$ / GeV
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d14-x01-y01
Title=Transverse region charged $p_\perp$ average
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT

# BEGIN PLOT /CDF_2010_I849042/d15-x01-y01
Title=Transverse region charged $p_\perp$ max
XLabel=$p_T(\mathrm{leading~jet})$ / GeV
LogY=0
# END PLOT
