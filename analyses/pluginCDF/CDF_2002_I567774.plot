# BEGIN PLOT /CDF_2002_I567774/d01-x01-y01
Title=Charged multiplicity at $\sqrt{s} = 630~\mathrm{GeV}$, $|\eta| < 1$, $p_T > 0.4~\mathrm{GeV}$
XLabel=$N_\mathrm{ch}$
YLabel=$\mathrm{d}{\sigma}/\mathrm{d}{n_\mathrm{ch}}$
FullRange=1
# END PLOT

# BEGIN PLOT /CDF_2002_I567774/d02-x01-y01
Title=Charged multiplicity at $\sqrt{s} = 1800~\mathrm{GeV}$, $|\eta| < 1$, $p_T > 0.4~\mathrm{GeV}$
XLabel=$N_\mathrm{ch}$
YLabel=$\mathrm{d}{\sigma}/\mathrm{d}{n_\mathrm{ch}}$
FullRange=1
# END PLOT

# BEGIN PLOT /CDF_2002_I567774/d03-x01-y01
Title=$\langle p_\perp \rangle$ vs. multiplicity at $\sqrt{s} = 630~\mathrm{GeV}$, $|\eta| < 1$, $p_T > 0.4~\mathrm{GeV}$
XLabel=$N_\mathrm{ch}$
YLabel=$\langle p_T \rangle$
LogY=0
LegendXPos=0.10
ShowZero=0
# END PLOT

# BEGIN PLOT /CDF_2002_I567774/d04-x01-y01
Title=$\langle p_\perp \rangle$ vs. multiplicity at $\sqrt{s} = 1800~\mathrm{GeV}$, $|\eta| < 1$, $p_T > 0.4~\mathrm{GeV}$
XLabel=$N_\mathrm{ch}$
YLabel=$\langle p_T \rangle$
LogY=0
LegendXPos=0.10
ShowZero=0
# END PLOT
