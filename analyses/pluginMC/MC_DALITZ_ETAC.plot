BEGIN PLOT /MC_DALITZ_ETAC/
LogY=0
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h01_pippim
Title=$\pi^+\pi^-$ mass distribution in $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h01_pipeta
Title=$\pi^+\eta$ mass distribution in $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m_{\pi^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h01_pimeta
Title=$\pi^-\eta$ mass distribution in $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m_{\pi^-\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz01
Title=Dalitz plot for $\eta_c\to \pi^+\pi^-\eta$
XLabel=$m^2_{\pi^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\eta}/\mathrm{d}m^2_{\pi^-\eta}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h02_pi0pi0
Title=$\pi^0\pi^0$ mass distribution in $\eta_c\to \pi^0\pi^0\eta$
XLabel=$m_{\pi^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h02_pi0eta
Title=$\pi^0\eta$ mass distribution in $\eta_c\to \pi^0\pi^0\eta$
XLabel=$m_{\pi^0\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz02
Title=Dalitz plot for $\eta_c\to \pi^0\pi^0\eta$
XLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\eta}/\mathrm{d}m^2_{\pi^0\eta}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h03_pippim
Title=$\pi^+\pi^-$ mass distribution in $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h03_pipeta
Title=$\pi^+\eta^\prime$ mass distribution in $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h03_pimeta
Title=$\pi^-\eta^\prime$ mass distribution in $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^-\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^-\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz03
Title=Dalitz plot for $\eta_c\to \pi^+\pi^-\eta^\prime$
XLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\eta^\prime}/\mathrm{d}m^2_{\pi^-\eta^\prime}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h04_pi0pi0
Title=$\pi^0\pi^0$ mass distribution in $\eta_c\to \pi^0\pi^0\eta^\prime$
XLabel=$m_{\pi^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h04_pi0eta
Title=$\pi^0\eta^\prime$ mass distribution in $\eta_c\to \pi^0\pi^0\eta^\prime$
XLabel=$m_{\pi^0\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz04
Title=Dalitz plot for $\eta_c\to \pi^0\pi^0\eta^\prime$
XLabel=$m^2_{\pi^0\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^0\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^0\eta^\prime}/\mathrm{d}m^2_{\pi^0\eta^\prime}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h05_KpKm
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\eta$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h05_Kpeta
Title=$K^+\eta$ mass distribution in $\eta_c\to K^+K^-\eta$
XLabel=$m_{K^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h05_Kmeta
Title=$K^-\eta$ mass distribution in $\eta_c\to K^+K^-\eta$
XLabel=$m_{K^-\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz05
Title=Dalitz plot for $\eta_c\to K^+K^-\eta$
XLabel=$m^2_{K^+\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\eta}/\mathrm{d}m^2_{K^-\eta}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h06_KS0KS0
Title=$K^0_SK^0_S$ mass distribution in $\eta_c\to K^0_SK^0_S\eta$
XLabel=$m_{K^0_SK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^0_S}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h06_KS0eta
Title=$K^0_S\eta$ mass distribution in $\eta_c\to K^0_SK^0_S\eta$
XLabel=$m_{K^0_S\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz06
Title=Dalitz plot for $\eta_c\to K^0_SK^0_S\eta$
XLabel=$m^2_{K^0_S\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\eta}/\mathrm{d}m^2_{K^0_S\eta}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h07_KL0KL0
Title=$K^0_LK^0_L$ mass distribution in $\eta_c\to K^0_LK^0_L\eta$
XLabel=$m_{K^0_LK^0_L}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_LK^0_L}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h07_KL0eta
Title=$K^0_L\eta$ mass distribution in $\eta_c\to K^0_LK^0_L\eta$
XLabel=$m_{K^0_L\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_L\eta}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz07
Title=Dalitz plot for $\eta_c\to K^0_LK^0_L\eta$
XLabel=$m^2_{K^0_L\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_L\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_L\eta}/\mathrm{d}m^2_{K^0_L\eta}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h08_KpKm
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h08_Kpeta
Title=$K^+\eta^\prime$ mass distribution in $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m_{K^+\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h08_Kmeta
Title=$K^-\eta^\prime$ mass distribution in $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m_{K^-\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz08
Title=Dalitz plot for $\eta_c\to K^+K^-\eta^\prime$
XLabel=$m^2_{K^+\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\eta^\prime}/\mathrm{d}m^2_{K^-\eta^\prime}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h09_KS0KS0
Title=$K^0_SK^0_S$ mass distribution in $\eta_c\to K^0_SK^0_S\eta^\prime$
XLabel=$m_{K^0_SK^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_SK^0_S}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h09_KS0eta
Title=$K^0_S\eta^\prime$ mass distribution in $\eta_c\to K^0_SK^0_S\eta^\prime$
XLabel=$m_{K^0_S\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz09
Title=Dalitz plot for $\eta_c\to K^0_SK^0_S\eta^\prime$
XLabel=$m^2_{K^0_S\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_S\eta^\prime}/\mathrm{d}m^2_{K^0_S\eta^\prime}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h10_KL0KL0
Title=$K^0_LK^0_L$ mass distribution in $\eta_c\to K^0_LK^0_L\eta^\prime$
XLabel=$m_{K^0_LK^0_L}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_LK^0_L}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h10_KL0eta
Title=$K^0_L\eta^\prime$ mass distribution in $\eta_c\to K^0_LK^0_L\eta^\prime$
XLabel=$m_{K^0_L\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_L\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz10
Title=Dalitz plot for $\eta_c\to K^0_LK^0_L\eta^\prime$
XLabel=$m^2_{K^0_L\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_L\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^0_L\eta^\prime}/\mathrm{d}m^2_{K^0_L\eta^\prime}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h11_KpKm
Title=$K^+K^-$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h11_Kppi0
Title=$K^+\pi^0$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m_{K^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h11_Kmpi0
Title=$K^-\pi^0$ mass distribution in $\eta_c\to K^+K^-\pi^0$
XLabel=$m_{K^-\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz11
Title=Dalitz plot for $\eta_c\to K^+K^-\pi^0$
XLabel=$m^2_{K^+\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^-\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^0}/\mathrm{d}m^2_{K^-\pi^0}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h12_KS0KS0
Title=$K_S^0K_S^0$ mass distribution in $\eta_c\to K_S^0K_S^0\pi^0$
XLabel=$m_{K_S^0K_S^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K_S^0K_S^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h12_KS0pi0
Title=$K_S^0\pi^0$ mass distribution in $\eta_c\to K_S^0K_S^0\pi^0$
XLabel=$m_{K_S^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K_S^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz12
Title=Dalitz plot for $\eta_c\to K_S^0K_S^0\pi^0$
XLabel=$m^2_{K_S^0\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K_S^0\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K_S^0\pi^0}/\mathrm{d}m^2_{K_S^0\pi^0}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h13_KL0KL0
Title=$K_L^0K_L^0$ mass distribution in $\eta_c\to K_L^0K_L^0\pi^0$
XLabel=$m_{K_L^0K_L^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K_L^0K_L^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h13_KL0pi0
Title=$K_L^0\pi^0$ mass distribution in $\eta_c\to K_L^0K_L^0\pi^0$
XLabel=$m_{K_L^0\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K_L^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz13
Title=Dalitz plot for $\eta_c\to K_L^0K_L^0\pi^0$
XLabel=$m^2_{K_L^0\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K_L^0\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K_L^0\pi^0}/\mathrm{d}m^2_{K_L^0\pi^0}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h14_KpKS0
Title=$K^+K^0_S$ mass distribution in $\eta_c\to K^+K^0_S\pi^-$
XLabel=$m_{K^+K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^0_S}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h14_Kppim
Title=$K^+\pi^-$ mass distribution in $\eta_c\to K^+K^0_S\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h14_KS0pim
Title=$K^0_S\pi^-$ mass distribution in $\eta_c\to K^+K^0_S\pi^-$
XLabel=$m_{K^0_S\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz14
Title=Dalitz plot for $\eta_c\to K^+K^0_S\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^-}/\mathrm{d}m^2_{K^0_S\pi^-}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h15_KmKS0
Title=$K^-K^0_S$ mass distribution in $\eta_c\to K^-K^0_S\pi^+$
XLabel=$m_{K^-K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-K^0_S}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h15_Kmpip
Title=$K^-\pi^+$ mass distribution in $\eta_c\to K^-K^0_S\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h15_KS0pip
Title=$K^0_S\pi^+$ mass distribution in $\eta_c\to K^-K^0_S\pi^+$
XLabel=$m_{K^0_S\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz15
Title=Dalitz plot for $\eta_c\to K^-K^0_S\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}/\mathrm{d}m^2_{K^0_S\pi^+}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h16_KpKL0
Title=$K^+K^0_L$ mass distribution in $\eta_c\to K^+K^0_L\pi^-$
XLabel=$m_{K^+K^0_L}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^0_L}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h16_Kppim
Title=$K^+\pi^-$ mass distribution in $\eta_c\to K^+K^0_L\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h16_KL0pim
Title=$K^0_L\pi^-$ mass distribution in $\eta_c\to K^+K^0_L\pi^-$
XLabel=$m_{K^0_L\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_L\pi^-}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz16
Title=Dalitz plot for $\eta_c\to K^+K^0_L\pi^-$
XLabel=$m^2_{K^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_L\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+\pi^-}/\mathrm{d}m^2_{K^0_L\pi^-}$ [$\mathrm{GeV}^{-4}$]
END PLOT

BEGIN PLOT /MC_DALITZ_ETAC/h17_KmKL0
Title=$K^-K^0_L$ mass distribution in $\eta_c\to K^-K^0_L\pi^+$
XLabel=$m_{K^-K^0_L}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-K^0_L}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.1
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h17_Kmpip
Title=$K^-\pi^+$ mass distribution in $\eta_c\to K^-K^0_L\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/h17_KL0pip
Title=$K^0_L\pi^+$ mass distribution in $\eta_c\to K^-K^0_L\pi^+$
XLabel=$m_{K^0_L\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_L\pi^+}$ [$\mathrm{GeV}^{-1}$]
LegendXPos=0.7
END PLOT
BEGIN PLOT /MC_DALITZ_ETAC/dalitz17
Title=Dalitz plot for $\eta_c\to K^-K^0_L\pi^+$
XLabel=$m^2_{K^-\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{K^0_L\pi^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^-\pi^+}/\mathrm{d}m^2_{K^0_L\pi^+}$ [$\mathrm{GeV}^{-4}$]
END PLOT
